<?php
/**
 * Cart
 * 
 * @author WebGladiolus
 * @version 1.0.0
 *
 * Methods description
 * 
 * - get - getting cart content
 * - add - add selected product to cart
 * - remove - removing selected product from cart
 * - empty - clear cart
 */
namespace Artamonov\Api\Controllers\v1;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Artamonov\Api\Request;
use Artamonov\Api\Response;
use Bitrix\Main\Loader;
use CModule;
use CSaleBasket;
use CSaleUser;
use CCatalogProduct;
use CCatalogProductProvider;
use CUser;
use CFile;

class Cart{

    /**
     * getting cart content
     *
     * @param token
     */
    public function get(){
        if (
           isset( $_REQUEST['token'] ) && strlen( $_REQUEST['token'] ) > 0
        ){
            CModule::IncludeModule('sale');
            CModule::IncludeModule('catalog');
            $rsUser = CUser::GetByID( $_REQUEST['token'] );
            $arUser = $rsUser->Fetch();
            $FUSER_ID=CSaleUser::GetList(array('USER_ID' => $arUser['ID']));

            $dbBasketItems = CSaleBasket::GetList(
                array(
                        "NAME" => "ASC",
                        "ID" => "ASC"
                    ),
                array(
                        "FUSER_ID" => $FUSER_ID['ID'],
                    ),
                false,
                false,
                array("ID", "CALLBACK_FUNC", "MODULE", 
                      "PRODUCT_ID", "QUANTITY", "DELAY", 
                      "CAN_BUY", "PRICE", "WEIGHT", "NAME", "ORDER_PRICE", 'LID')
            );

            $arResult = [];
            $total = 0;
            while ($arItem = $dbBasketItems->Fetch()) {
                $arProductData = CCatalogProduct::GetByIDEx(  $arItem['PRODUCT_ID'] );
                $arItems[] = [
                    'product_id' => $arItem['PRODUCT_ID'],
                    'id' => $arItem['ID'],
                    'quantity' => $arItem['QUANTITY'],
                    'title' => $arItem['NAME'],
                    'description' => 'Some description',
                    'price' => $arItem['PRICE'],
                    // 'lid' => $arItem['LID'],
                    'rating' => 0,
                    'image' => CFile::GetPath( $arProductData['PREVIEW_PICTURE'] ),
                    'options' => [
                        'color' => [
                            'red',
                        ],
                        'size' => [
                            'XL',
                        ],
                        'weight' => $arItem['WEIGHT'],
                    ],
                ];

                if( is_numeric( $arItem['QUANTITY'] ) && is_numeric( $arItem['PRICE'] ) ){
                    $total += ( $arItem['QUANTITY'] * $arItem['PRICE'] );
                }
            }
        }

    	$resp = [ 
    		'code' => 200,
    		'message' => 'Cart received successfully',
    		'data' => [
    			'items' => $arItems,
                'total' => $total,
    		],
    	];

        Response::ShowResult( $resp, JSON_UNESCAPED_UNICODE );
    }

    /**
     * adding item to the cart
     *
     * @param token
     * @param id - id of the product
     */
    public function add(){
        CModule::IncludeModule('sale');
        CModule::IncludeModule('catalog');

        $product_id = $_REQUEST['prod_id'];
        $token = $_REQUEST['token'];
        $uid = $token;

        $product = CCatalogProduct::GetByIDEx( $product_id );

        $arFields = [
            'PRODUCT_ID' => $product_id,
            'PRICE' => 300,//$product['PRICES'][1]['PRICE'],
            'CURRENCY' => 'UAH',
            'LID' => 's1',
            'NAME' => $product['NAME'],
            'FUSER_ID' => CSaleUser::GetList(array('USER_ID' => $uid))['ID'],
        ];
        $x = CSaleBasket::Add( $arFields );

        $args = [
            'code' => 200,
            'message' => 'Product successfully added to cart',
        ];

        Response::ShowResult( $arFields, JSON_UNESCAPED_UNICODE );
    }

    /**
     * removing item to the cart
     *
     * @param token
     * @param id - id of the product
     */
    public function remove(){
        $args = [
            'code' => 200,
            'message' => 'Product successfully removed from cart',
        ];
        Response::ShowResult( $args, JSON_UNESCAPED_UNICODE );
    }

    /**
     * clear user cart
     * 
     * @param $token - user token
     */
    public function empty(){
        $args = [
            'code' => 200,
            'message' => 'Cart empty',
        ];
        Response::ShowResult( $args, JSON_UNESCAPED_UNICODE );
    }
}