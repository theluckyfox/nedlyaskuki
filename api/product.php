<?php
/**
 * Product
 * 
 * @author WebGladiolus
 * @version 1.0.0
 *
 * Methods description
 *
 * - get - getting product by id
 * - getList - getting first 10 products
 * - getBySection - getting products by section id
 * - toFavourite - add/remove product to user's favourite 
 */
namespace Artamonov\Api\Controllers\v1;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Artamonov\Api\Request;
use Artamonov\Api\Response;
use Bitrix\Main\Loader;
use CModule;
use CSaleBasket;
use CSaleUser;
use CCatalogProduct;
use CCatalogProductProvider;
use CUser;
use CIBlockElement;
use CIBlockSection;
use CFile;
use CPrice;
use CRatings;
use CForumMessage;

class Product{

    /**
     * getting product details
     *
     * @param prod_id - id of the required product
     */
    public function get(){
        $product_id = $_REQUEST['prod_id'];
        if( empty( $product_id ) ){
            Response::BadRequest( 'Product id required' );
            return;
        }

        CModule::IncludeModule('iblock');
        CModule::IncludeModule('catalog');
        CModule::IncludeModule("forum");
        $arProductData = CCatalogProduct::GetByIDEx( $product_id );
        if( !empty( $arProductData ) ){

            // getting related products
            Loader::includeModule('iblock');
            $arFilter = [
                'IBLOCK_ID' => 1,
                'SECTION_ID' => $arProductData['IBLOCK_SECTION_ID'],
                'ACTIVE'=>'Y',
            ];

            $arSelect = Array('ID', 'NAME', 'PREVIEW_PICTURE', 'PREVIEW_TEXT', 'DETAIL_PICTURE', 'DETAIL_TEXT', 'DATE_CREATE' );
            $res = CIBlockElement::GetList( Array(), $arFilter, false, Array("nPageSize"=>15), $arSelect );
            
            $arRelProd = [];
            while($ob = $res->GetNextElement())
            {
                $arX = $ob->GetFields();
                $arR['id'] = $arX['ID'];
                $arR['title'] = $arX['NAME'];
                $arR['price'] = CPrice::GetBasePrice( $arX['ID'] )['PRICE'];
                $arR['currency'] = CPrice::GetBasePrice( $arX['ID'] )['CURRENCY'];
                $arR['rating'] = 4;
                $arR['preview_picture'] = CFile::GetPath( $arX['PREVIEW_PICTURE'] );
                // $arR['preview_text'] = $arX['PREVIEW_TEXT'];
                $arRelProd[] = $arR;
            }

            // filling response array
            $arResult = [
                'product_id' => $product_id,
                'date_create' => $arProductData['DATE_CREATE'],
                'title' => $arProductData['NAME'],
                'preview_text' => $arProductData['PREVIEW_TEXT'],
                'preview_picture' => CFile::GetPath( $arProductData['PREVIEW_PICTURE'] ),
                'detail_text' => $arProductData['DETAIL_TEXT'],
                'detail_picture' => CFile::GetPath( $arProductData['DETAIL_PICTURE'] ),
                'weight' => $arProductData['PRODUCT']['WEIGHT'],
                'width' => $arProductData['PRODUCT']['WIDTH'],
                'height' => $arProductData['PRODUCT']['HEIGHT'],
                'measure' => $arProductData['PRODUCT']['MEASURE'],
                'price' => $arProductData['PRICES'][1]['PRICE'],
                'currency' => $arProductData['PRICES'][1]['CURRENCY'],
                'rating' => 4,
                'comments' => [
                    [
                        'uid' => 1,
                        'user_name' => 'Алина Сладенькая',
                        'rating' => 4,
                        'text' => 'Отличная вещица!',
                        'date' => '14-05-2018 15:40',
                    ],
                ],
                'related_products' => $arRelProd,
            ];
            // $db_res = CForumMessage::GetList(array("ID"=>"ASC"), array("FORUM_ID"=>1));
            // while ($ar_res = $db_res->Fetch())
            // {
            //   $arResult['comments'][] =  $ar_res;
            // }
        }
        Response::ShowResult( $arResult, JSON_UNESCAPED_UNICODE );
    }

    /**
     * getting random 10 products
     */
    public function getList(){
        CModule::IncludeModule('catalog');
        $db_res = CCatalogProduct::GetList(
            array("QUANTITY" => "DESC"),
            array("QUANTITY_TRACE" => "N"),
            false,
            array("nTopCount" => 10)
        );

        $ind = 0;
        $arResult = [];
        while (($ar_res = $db_res->Fetch()) && ($ind < 10))
        {
            // $arResult[] = $ar_res;
            $arProductData = CCatalogProduct::GetByIDEx( $ar_res['ID'] );
            if( !empty( $arProductData ) ){
                $arResult[] = [
                    'product_id' => $ar_res['ID'],
                    'date_create' => $arProductData['DATE_CREATE'],
                    'title' => $arProductData['NAME'],
                    'preview_text' => $arProductData['PREVIEW_TEXT'],
                    'preview_picture' => CFile::GetPath( $arProductData['PREVIEW_PICTURE'] ),
                    'detail_text' => $arProductData['DETAIL_TEXT'],
                    'detail_picture' => CFile::GetPath( $arProductData['DETAIL_PICTURE'] ),
                    'weight' => $arProductData['PRODUCT']['WEIGHT'],
                    'width' => $arProductData['PRODUCT']['WIDTH'],
                    'height' => $arProductData['PRODUCT']['HEIGHT'],
                    'measure' => $arProductData['PRODUCT']['MEASURE'],
                    'price' => $arProductData['PRICES'][1]['PRICE'],
                    'currency' => $arProductData['PRICES'][1]['CURRENCY'],
                ];
            }
            $ind++;
        }
        Response::ShowResult( $arResult, JSON_UNESCAPED_UNICODE );
    }

    /**
     * getting products list by section id
     *
     * @param id - id of the required section
     */
    public function getBySection(){
        $sectionId = $_REQUEST['id'];
        if( empty( $sectionId ) ){
            Response::BadRequest( 'Section id required' );
            return;
        }
        
        Loader::includeModule('iblock');
        $arFilter = [
            'IBLOCK_ID' => 1,
            'SECTION_ID' => $sectionId,
            'ACTIVE'=>'Y',
        ];
        $arSelect = Array('ID', 'NAME', 'PREVIEW_PICTURE', 'PREVIEW_TEXT', 'DETAIL_PICTURE', 'DETAIL_TEXT', 'DATE_CREATE' );
        // , 'PREVIEW_PICTURE', 'PREVIEW_TEXT', 'DETAIL_PICTURE', 'DETAIL_TEXT', 'DATE_CREATE'
        $res = CIBlockElement::GetList( Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect );
        while($ob = $res->GetNextElement())
        {
            // $arResult[] = $ob->GetFields();
            $arX = $ob->GetFields();
            $arR['id'] = $arX['ID'];
            $arR['title'] = $arX['NAME'];
            $arR['price'] = CPrice::GetBasePrice( $arX['ID'] )['PRICE'];
            $arR['currency'] = CPrice::GetBasePrice( $arX['ID'] )['CURRENCY'];
            $arR['rating'] = 4;
            $arR['preview_picture'] = CFile::GetPath( $arX['PREVIEW_PICTURE'] );
            $arR['preview_text'] = $arX['PREVIEW_TEXT'];
            $arResult[] = $arR;
        }
        Response::ShowResult($arResult, JSON_UNESCAPED_UNICODE);   
    }

    /**
     * adding product to favorites
     *
     * @param $token - id of the user [ need to be replaced eith token ]
     * @param $prod_id - id of the product
     */
    public function toFavourites(){
        $token = $_REQUEST['token'];
        $prodId = $_REQUEST['prod_id'];
        if( empty( $token ) || empty( $prodId ) ){
            Response::BadRequest( 'Token and product id required' );
            return;
        }

        $uid = $token;

        $rsUser = CUser::GetByID($uid);
        $arUser = $rsUser->Fetch();
        $arElements = unserialize($arUser['UF_NL_FAVORITES']);
        if ($arElements) {
            $e = false;
            $_id = 0;
            foreach ( $arElements as $key => $data ) {
                if ( array_key_exists( $prodId, $data ) ) {
                    $e = true;
                    $_id = $key;
                    break;
                }
            }

            if( $e ){
                unset( $arElements[$_id] );
                $message = 'Product removed from favourites';
            }
            else{
                $arElements[] = [ $prodId => 1 ];
                $message = 'Product added to favourites';
            }

            ( new CUser )->Update( $uid, Array( "UF_NL_FAVORITES" => serialize( $arElements ) ) );
            Response::ShowResult($message, JSON_UNESCAPED_UNICODE);
        }
        Response::NoResult('Internal error', JSON_UNESCAPED_UNICODE);
    }
}