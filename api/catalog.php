<?php
/**
 * Catalog
 * 
 * @author WebGladiolus
 * @version 1.0.0
 */
namespace Artamonov\Api\Controllers\v1;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Artamonov\Api\Request;
use Artamonov\Api\Response;
use Bitrix\Main\Loader;
use CModule;
use CSaleBasket;
use CSaleUser;
use CCatalogProduct;
use CCatalogProductProvider;
use CUser;
use CIBlockElement;
use CIBlockSection;
use CFile;

class Catalog{
    /**
     * getting the list of main sections on the site
     */
    public function getSections(){
        Loader::includeModule('iblock');
        $arFilter = array('IBLOCK_ID' => 1,'ACTIVE'=>'Y', 'DEPTH_LEVEL' => 1);
        $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
        while ($arSection = $rsSections->Fetch())
        {
            $arF = [
                "IBLOCK_ID"=>1,
                "SECTION_ID"=>$arSection['ID']
            ];
            $arSectionPrepared['id'] = $arSection['ID'];
            $arSectionPrepared['created'] = $arSection['DATE_CREATE'];
            $arSectionPrepared['title'] = $arSection['NAME'];
            $arSectionPrepared['count'] = CIBlockSection::GetCount($arF);
            $arSectionPrepared['image'] = $arSection['PICTURE'];
            $arResult[] = $arSectionPrepared;
        }
        Response::ShowResult($arResult, JSON_UNESCAPED_UNICODE);
    }

    /**
     * getting the list of sub-sections of the one of main sections
     */
    public function getSubSections(){
        Loader::includeModule('iblock');
        $sectionId = $_REQUEST['id'];
        $arFilter = array('IBLOCK_ID' => 1,'ACTIVE'=>'Y', 'SECTION_ID' => $sectionId );
        $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
        while ($arSection = $rsSections->Fetch())
        {
            $arSectionPrepared['id'] = $arSection['ID'];
            $arSectionPrepared['created'] = $arSection['DATE_CREATE'];
            $arSectionPrepared['title'] = $arSection['NAME'];
            $arSectionPrepared['image'] = $arSection['PICTURE'];
            $arResult[] = $arSectionPrepared;
        }
        Response::ShowResult($arResult, JSON_UNESCAPED_UNICODE);
    }

    /**
     * getting the list of all sections with subsections
     */
    public function getSectionsMap(){
        Loader::includeModule('iblock');
        $sectionId = $_REQUEST['id'];
        $arFilter = array('IBLOCK_ID' => 1,'ACTIVE'=>'Y', 'DEPTH_LEVEL' => 2, 'IBLOCK_SECTION_ID' => $sectionId );
        $rsSections = CIBlockSection::GetList(array('LEFT_MARGIN' => 'ASC'), $arFilter);
        while ($arSection = $rsSections->Fetch())
        {
            $arSectionPrepared['id'] = $arSection['ID'];
            $arSectionPrepared['created'] = $arSection['DATE_CREATE'];
            $arSectionPrepared['title'] = $arSection['NAME'];
            $arSectionPrepared['image'] = $arSection['PICTURE'];
            $arResult[] = $arSectionPrepared;
        }
        Response::ShowResult($arResult, JSON_UNESCAPED_UNICODE);
    }
}