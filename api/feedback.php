<?php
/**
 * Feedback
 * 
 * @author WebGladiolus
 * @version 1.0.0
 */
namespace Artamonov\Api\Controllers\v1;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Artamonov\Api\Request;
use Artamonov\Api\Response;
use Bitrix\Main\Loader;

class Feedback{

    /**
     * getting cart content
     *
     * @param token
     */
    public function send(){
    	$args = [ 
    		'code' => 200,
    		'message' => 'Feedback sent syccessfully',
    	];
        Response::ShowResult( $args, JSON_UNESCAPED_UNICODE );
    }
}