<?php
/**
 * User
 * 
 * @author WebGladiolus
 * @version 1.0.0
 *
 * Methods description
 * 
 * - login - check the user login and password
 * - fblogin - login via facebook
 * - ilogin - login via instagram
 * - register - create new user
 * - getFavourites - getting the favourite user's products
 * - info - getting information about user by token
 * - update - updating user information
 * - changePass - changing the password
 */
namespace Artamonov\Api\Controllers\v1;
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Artamonov\Api\Request;
use Artamonov\Api\Response;
use Bitrix\Main\Loader;
use CUser;

class User{

	/**
	 * Standard login via login+password
	 * 
	 * @param login
	 * @param password
	 */
    public function login(){

        $login_password_correct = false;

        if (
           isset( $_REQUEST['login'] ) && strlen( $_REQUEST['password'] ) > 0
        &&
           isset( $_REQUEST['login'] ) && strlen( $_REQUEST['password'] ) > 0
        )
        {

           $rsUser = CUser::GetByLogin( $_REQUEST['login'] );
           if ($arUser = $rsUser->Fetch())
           {
              if(strlen($arUser["PASSWORD"]) > 32)
              {
                 $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
                 $db_password = substr($arUser["PASSWORD"], -32);
              }
              else
              {
                 $salt = "";
                 $db_password = $arUser["PASSWORD"];
              }

              $user_password =  md5($salt.$_REQUEST['password']);

              if ( $user_password == $db_password )
              {
                $token = sha1( time() );
                $usr = new CUser();
                $usr->update( $arUser['ID'], ['UF_API_TOKEN' => $token] );
                $args = [ 
                  'code' => 200,
                  'message' => 'Login success',
                  'data' => [
                      'id' => $arUser['ID'],
                      'name' => $arUser['LAST_NAME'],
                      'email' => $arUser['EMAIL'],
                      'phone' => $arUser['PERSONAL_PHONE'],
                      'photo' => $arUser['PERSONAL_PHOTO'],
                      'token' => $token,
                  ],
                ];
                Response::ShowResult( $args, JSON_UNESCAPED_UNICODE );
              }
              else{
                $resp = [
                  'code' => 400,
                  'message' => 'Login failed',
                ];
                Response::ShowResult( $resp, JSON_UNESCAPED_UNICODE );
              }
           }
        }
    }

    /**
     * Login via facebook
     *
     * @param name - user name
     * @param email - user email
     * @param fbkey - facebook profile id
     */
    public function fblogin(){
      $rsUser = CUser::GetByLogin( $_REQUEST['email'] );
      $arUser = $rsUser->Fetch();
      $token = sha1( time() );

      if( !$arUser ){
        $user = new CUser;
        $arFields = Array(
          "NAME"              => $_REQUEST['name'],
          "EMAIL"             => $_REQUEST['email'],
          "LOGIN"             => $_REQUEST['email'],
          "ACTIVE"            => "Y",
          "PASSWORD"          => $_REQUEST['email'],
          "CONFIRM_PASSWORD"  => $_REQUEST['email'],
          "UF_FB_KEY"  => $_REQUEST['key'],
          "UF_API_TOKEN"  => $token,
        );

        $ID = $user->Add($arFields);
        $resp = [
          'code' => 200, 
          'message' => 'Facebook login success. Created new user.', 
          'data'=>[
            'token' => $token,
          ]
        ];
        Response::ShowResult( $resp, JSON_UNESCAPED_UNICODE );
        return;
      }

      $usr = new CUser();
      $usr->update( $arUser['ID'], ['UF_API_TOKEN' => $token] );
      
      $resp = [
        'code' => 400, 
        'message' => 'Facebook login success',
        'data'=>[
            'token' => $token,
        ],
      ]; 
      Response::ShowResult( $resp, JSON_UNESCAPED_UNICODE );
    }

    /**
     * login via instagram
     *
     * @param instaid - instagram profile id
     * @param name - user name
     * @param email - user email
     */
    public function ilogin(){
    	$rsUser = CUser::GetByLogin( $_REQUEST['email'] );
      $arUser = $rsUser->Fetch();
      $token = sha1( time() );

      if( !$arUser ){
        $user = new CUser;
        $arFields = Array(
          "NAME"              => $_REQUEST['name'],
          "EMAIL"             => $_REQUEST['email'],
          "LOGIN"             => $_REQUEST['email'],
          "ACTIVE"            => "Y",
          "PASSWORD"          => $_REQUEST['email'],
          "CONFIRM_PASSWORD"  => $_REQUEST['email'],
          "UF_INSTA_KEY"  => $_REQUEST['key'],
          "UF_API_TOKEN"  => $token,
        );

        $ID = $user->Add($arFields);
        $resp = [
          'code' => 200, 
          'message' => 'Instagram login success. Created new user.', 
          'data'=>[
            'token' => $token,
          ]
        ];
        Response::ShowResult( $resp, JSON_UNESCAPED_UNICODE );
        return;
      }
      
      $usr = new CUser();
      $usr->update( $arUser['ID'], ['UF_API_TOKEN' => $token] );
      
      $resp = [
        'code' => 400, 
        'message' => 'Instagram login success',
        'data'=>[
            'token' => $token,
        ],
      ]; 
      Response::ShowResult( $resp, JSON_UNESCAPED_UNICODE );
    }

    /**
     * creating new user
     * 
     * @param $login - user login
     * @param $email - user email
     * @param $pass - user password
     * @param $passconfirm - repeat the password
     * @param $name - user name
     * @param $lastname - user lastname
     */
    public function register(){
      if( !isset( $_REQUEST['login'] ) || empty( $_REQUEST['login'] ) ){
        Response::ShowResult( 'Login required and login can\'t be empty.', JSON_UNESCAPED_UNICODE );
      }
      $login = $_REQUEST['login'];

      if( !isset( $_REQUEST['pass'] ) || empty( $_REQUEST['pass'] ) ){
        Response::ShowResult( 'Password required and password can\'t be empty.', JSON_UNESCAPED_UNICODE );
      }
      $pass = $_REQUEST['pass'];

      if( !isset( $_REQUEST['passconfirm'] ) || empty( $_REQUEST['passconfirm'] ) ){
        Response::ShowResult( 'Password confirmation required and paswword confirmation can\'t be empty.', JSON_UNESCAPED_UNICODE );
      }
      $passConfirm = $_REQUEST['passconfirm'];

      if( !isset( $_REQUEST['name'] ) || empty( $_REQUEST['name'] ) ){
        Response::ShowResult( 'Name required and Name can\'t be empty.', JSON_UNESCAPED_UNICODE );
      }
      $name = $_REQUEST['name'];

      if( !isset( $_REQUEST['lastname'] ) || empty( $_REQUEST['lastname'] ) ){
        Response::ShowResult( 'Last name required and last name can\'t be empty.', JSON_UNESCAPED_UNICODE );
      }
      $lastname = $_REQUEST['lastname'];

      if( !isset( $_REQUEST['email'] ) || empty( $_REQUEST['email'] ) ){
        Response::ShowResult( 'Email required and email can\'t be empty.', JSON_UNESCAPED_UNICODE );
      }
      $email = $_REQUEST['email'];

      $user = new CUser;
        $arFields = Array(
          "NAME"              => $_REQUEST['name'],
          "LAST_NAME"         => $_REQUEST['lastname'],
          "EMAIL"             => $_REQUEST['email'],
          "LOGIN"             => $_REQUEST['login'],
          "ACTIVE"            => "Y",
          "PASSWORD"          => $_REQUEST['pass'],
          "CONFIRM_PASSWORD"  => $_REQUEST['passconfirm'],
        );

        $ID = $user->Add($arFields);
        if (intval($ID) > 0){
          $resp = [
            'code' => 200,
            'message' => 'User added successfully.',
          ];
          Response::ShowResult( $resp, JSON_UNESCAPED_UNICODE );
        }
        else{
          $resp = [
            'code' => 200,
            'message' => $user->LAST_ERROR,
          ];
          Response::ShowResult( $resp, JSON_UNESCAPED_UNICODE );
        }
    }

    /**
     * getting user's favourite products
     *
     * @param $token - user token
     */
    public function getFavourites(){
      $token = $_REQUEST['token'];
      if( empty( $token ) ){
        $resp = [
            'code' => 400,
            'message' => 'Token required',
        ];
        Response::BadRequest( $resp );
        return;
      }

      $uid = $token;

      $rsUser = CUser::GetByID($uid);
      $arUser = $rsUser->Fetch();
      $arFavorite = unserialize($arUser['UF_NL_FAVORITES']);
      if ($arFavorite) {
        $resp = [
          'code' => 200,
          'message' => 'List of favourite products',
          'data' => $arFavorite,
        ];
        Response::ShowResult($resp, JSON_UNESCAPED_UNICODE);
      }
      $resp = [
        'code' => 400,
        'message' => 'Internal error',
      ];
      Response::NoResult($resp, JSON_UNESCAPED_UNICODE);
    }

    /**
     * getting information about user
     *
     * @param $token - user token
     */
    public function info(){

    }

    /**
     * updating user information
     *
     * @param $token - user token
     */
    public function update(){
      $rsUser = CUser::GetByID( $_REQUEST['token'] );
      $arUser = $rsUser->Fetch();

      $usr = new CUser();
      $fields = Array(
        "NAME"              => $_REQUEST['name'],
        "LAST_NAME"         => $_REQUEST['lastname'],
        "EMAIL"             => $_REQUEST['email'],
      );
      $usr->update( $arUser['ID'], $fields );
      $resp = [
        'code' => 200,
        'message' => 'User information updated successful',
      ];
      Response::ShowResult($resp, JSON_UNESCAPED_UNICODE);
    }

    /**
     * updating user information
     *
     * @param $token - user token
     */
    public function changePass(){
      $rsUser = CUser::GetByID( $_REQUEST['token'] );
      $arUser = $rsUser->Fetch();

      $pass = $_REQUEST['pass'];
      $passConfirm = $_REQUEST['pass_confirm'];

      $usr = new CUser();
      $fields = Array(
        "PASSWORD"          => $pass,
        "CONFIRM_PASSWORD"  => $passConfirm,
      );
      $usr->update( $arUser['ID'], $fields );
      $resp = [
        'code' => 200,
        'message' => 'Password changed successful',
      ];
      Response::ShowResult($resp, JSON_UNESCAPED_UNICODE);
    }
}